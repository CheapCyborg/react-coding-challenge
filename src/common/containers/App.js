import React, { useState, useEffect } from 'react';
import AppContext from '../../routes/App/components/AppContext';
import '../../styles/_dark-mode.scss';

export default function App({ children }) {
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    if (darkMode) {
      document.getElementById('root').classList.add('dark-mode');
    } else {
      document.getElementById('root').classList.remove('dark-mode');
    }
  });

  const changeTheme = () => {
    setDarkMode(!darkMode);
  };

  const darkModeToggle = {
    enabled: darkMode,
    toggle: changeTheme,
  };

  return (
    <AppContext.Provider value={darkModeToggle}>
      {children}
    </AppContext.Provider>
  );
}
